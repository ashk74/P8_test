<?php

namespace App\Tests\Entity;

use App\Entity\Product;
use PHPUnit\Framework\TestCase;

class ProductTest extends TestCase
{
    /**
     * @dataProvider pricesForFoodProduct
     */
    public function testComputeTVAFoodProduct($price, $expectedTva): void
    {
        $product = new Product('Sushi', Product::FOOD_PRODUCT, $price);
        $result = $product->computeTVA();

        $this->assertSame($expectedTva, $result);
    }

    public function pricesForFoodProduct()
    {
        return [
            [0, 0.0],
            [20, 1.1],
            [100, 5.5]
        ];
    }

    public function testComputeTVAProductOtherType(): void
    {
        $product = new Product('iPhone', 'Smartphone', 1000);
        $result = $product->computeTVA();

        $this->assertSame(196.0, $result);
    }

    public function testNegativePriceComputeTVA(): void
    {
        $product = new Product('iPhone', 'Smartphone', -1);
        $this->expectException('LogicException');
        $result = $product->computeTVA();
    }
}
